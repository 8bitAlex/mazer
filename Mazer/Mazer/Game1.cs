using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Mazer.Sprites;
using Mazer.HUDManager;

namespace Mazer
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        SpriteManager spriteManager;
        HUDoverlay overlay;

        Texture2D background;
        Texture2D scoreBoard;
        SpriteFont font;

        int level;
        const int MAX_LEVEL = 5;
        Boolean showScoreScreen;

        //Audio
        AudioEngine audioEngine;
        WaveBank waveBank;
        SoundBank soundBank;


        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            level = 0;
            showScoreScreen = false;

            graphics.IsFullScreen = false;
            graphics.PreferredBackBufferHeight = 600;
            graphics.PreferredBackBufferWidth = 800;
            graphics.ApplyChanges();
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            spriteManager = new SpriteManager(this,level);
            overlay = new HUDoverlay(this);

            Components.Add(spriteManager);
            Components.Add(overlay);

            overlay.setLevel(level);
            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            background = Content.Load<Texture2D>(@"images/levels/level template");
            scoreBoard = Content.Load<Texture2D>(@"images/instructions/high score");
            font = Content.Load<SpriteFont>(@"fonts/TimerFont");

            audioEngine = new AudioEngine(@"content\audio\GameAudio.xgs");
            waveBank = new WaveBank(audioEngine, @"content\audio\wave bank.xwb");
            soundBank = new SoundBank(audioEngine, @"content\audio\Sound Bank.xsb");

            soundBank.PlayCue("Background Audio");
            // TODO: use this.Content to load your game content here
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (spriteManager.isGameOver(gameTime))
            {
                level++;
                if (level == 1)
                {
                    soundBank.PlayCue("Start");
                    overlay.reset(); //start the game
                }
                else
                    soundBank.PlayCue("Score");
                
                System.Threading.Thread.Sleep(1000);
                overlay.setLevel(level);
                reset();
            }

            if (spriteManager.wallCollision())
            {
                soundBank.PlayCue("Collision");
                reset();
                overlay.addAttempt();
            }
            if (level == MAX_LEVEL)
            {
                displayScore();
            }

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.White);

            spriteBatch.Begin();

            if (showScoreScreen)
            {
                overlay.drawScore();
            }
                
            spriteBatch.Draw(background, new Rectangle(0, 0, 800, 600),null, Color.White,0.0f, Vector2.Zero,SpriteEffects.None,1);

            spriteBatch.End();

            base.Draw(gameTime);
        }

        private void reset()
        {
            Components.Remove(spriteManager);
            spriteManager = new SpriteManager(this, level);
            Components.Add(spriteManager);
        }

        private void displayScore()
        {
            showScoreScreen = true;
            overlay.stopTimer();
            overlay.drawScore();
            endMenu();
        }

        private void endMenu()
        {
            if (Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();
            if (Keyboard.GetState().IsKeyDown(Keys.Enter))
            {
                level = 0;
                showScoreScreen = false;
                overlay.hideScore();
                overlay.reset();
                overlay.setLevel(level);
                reset();
            }
        }

    }
}
