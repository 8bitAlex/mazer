using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Mazer.Levels;
using Mazer.HUDManager;


namespace Mazer.Sprites
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public class SpriteManager : Microsoft.Xna.Framework.DrawableGameComponent
    {

        int level;
        Game game;
        LevelData data;
        SpriteBatch spriteBatch;
        List<Sprite> spriteList = new List<Sprite>();
        List<Sprite> playerList = new List<Sprite>();

        public SpriteManager(Game game,int level)
            : base(game)
        {
            this.level = level;
            this.game = game;
        }

        /// <summary>
        /// Allows the game component to perform any initialization it needs to before starting
        /// to run.  This is where it can query for any required services and load content.
        /// </summary>
        public override void Initialize()
        {
            // TODO: Add your initialization code here
            data = new LevelData(game);

            base.Initialize();
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(Game.GraphicsDevice);

            data.loadSprites(playerList, spriteList, level);

            base.LoadContent();
        }

        /// <summary>
        /// Allows the game component to update itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime)
        {
            updatePlayer(gameTime);

            playerSpriteContact();

            updateSprites(gameTime);

            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            spriteBatch.Begin(SpriteSortMode.FrontToBack, BlendState.AlphaBlend);

            foreach (Sprite p in playerList)
            {
                p.Draw(gameTime, spriteBatch);
            }

            foreach (Sprite s in spriteList)
            {
                s.Draw(gameTime, spriteBatch);
            }

            spriteBatch.End();
            base.Draw(gameTime);
        }

        public Boolean isGameOver(GameTime gameTime)
        {
            foreach (Sprite s in spriteList)
            {
                if (s is exitBlock && playerList.First().collisionRect.Intersects(s.collisionRect)
                    && allScoreBlocksConnected())
                {
                    playerList.First().stop();
                    return true;
                }
                    
            }

            return false;
        }

        public Boolean wallCollision()
        {
            foreach (Sprite s in spriteList)
            {
                foreach (Sprite p in playerList)
                {
                    if (s is wallBlock && p.collisionRect.Intersects(s.collisionRect))
                    {
                        playerList.First().stop();
                        return true;
                    }
                }
            }

            return false;
        }

///****************************************************************************
///                              Private Methods
///****************************************************************************

        private void updatePlayer(GameTime gametime)
        {
            foreach (Sprite s in playerList)
            {
                s.Update(gametime, Game.Window.ClientBounds);
            }
        }

        private void updateSprites(GameTime gametime)
        {
            foreach (Sprite s in spriteList)
            {
                s.Update(gametime, Game.Window.ClientBounds);
            }
        }

        private void playerSpriteContact()
        {
            List<Sprite> temp = new List<Sprite>();

            foreach (Sprite s in spriteList)
            {
                foreach (Sprite p in playerList)
                {
                    if ((s is ScoreBlock || s is WeightBlock) && s.collisionRect.Intersects(p.collisionRect))
                    {
                        s.setConnected();
                        temp.Add(s);

                        //if(p is UserControlledSprite) p.drawFace(new Point(1, 2));
                        (playerList.First()).drawFace(new Point(1,2));
                    }
                }
            }

            foreach (Sprite t in temp)
            {
                if (!playerList.Contains(t))
                {
                    spriteList.Remove(t);
                    playerList.Add(t);
                }
                
            }
        }

        private Boolean allScoreBlocksConnected()
        {
            foreach (Sprite s in spriteList)
            {
                if (s is ScoreBlock) return false;
            }

            return true;
        }
    }
}
