﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Mazer.Sprites
{
    class ConnectableSprite : Sprite
    {

        private Boolean isConnected = false;

        public ConnectableSprite(Texture2D textureImage, Vector2 position ,Point frameSize,
            int collisionOffset, Point currentFrame, Point sheetSize, Vector2 speed) :
            base(textureImage, position, frameSize, collisionOffset, currentFrame, sheetSize, speed)
        {

        }

        public ConnectableSprite(Texture2D textureImage, Vector2 position ,Point frameSize,
            int collisionOffset, Point currentFrame, Point sheetSize, Vector2 speed, int millisecondsPerFrame)
            : base(textureImage, position, frameSize, collisionOffset, currentFrame, sheetSize, speed,
            millisecondsPerFrame)
        {

        }

        public override Vector2 Direction
        {
            get
            {
                if (isConnected)
                {
                    Vector2 inputDirection = Vector2.Zero;

                    if (Keyboard.GetState().IsKeyDown(Keys.Left) || Keyboard.GetState().IsKeyDown(Keys.A))
                        inputDirection.X -= 1;
                    if (Keyboard.GetState().IsKeyDown(Keys.Right) || Keyboard.GetState().IsKeyDown(Keys.D))
                        inputDirection.X += 1;
                    if (Keyboard.GetState().IsKeyDown(Keys.Up) || Keyboard.GetState().IsKeyDown(Keys.W))
                        inputDirection.Y -= 1;
                    if (Keyboard.GetState().IsKeyDown(Keys.Down) || Keyboard.GetState().IsKeyDown(Keys.S))
                        inputDirection.Y += 1;

                    GamePadState gamepadState = GamePad.GetState(PlayerIndex.One);
                    if (gamepadState.ThumbSticks.Left.X != 0)
                        inputDirection.X += gamepadState.ThumbSticks.Left.X;
                    if (gamepadState.ThumbSticks.Left.Y != 0)
                        inputDirection.Y -= gamepadState.ThumbSticks.Left.Y;
                
                    return inputDirection * speed;
                }
                return Vector2.Zero;

            }
        }

        public override void setConnected()
        {
            this.isConnected = true;
        }

    }
}
