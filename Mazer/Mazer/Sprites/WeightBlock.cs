﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;

namespace Mazer.Sprites
{
    class WeightBlock : ConnectableSprite
    {

        public WeightBlock(Texture2D textureImage, Vector2 position ,Point frameSize,
            int collisionOffset, Point currentFrame, Point sheetSize, Vector2 speed) :
            base(textureImage, position, frameSize, collisionOffset, currentFrame, sheetSize, speed)
        {

        }

        public WeightBlock(Texture2D textureImage, Vector2 position ,Point frameSize,
            int collisionOffset, Point currentFrame, Point sheetSize, Vector2 speed, int millisecondsPerFrame)
            : base(textureImage, position, frameSize, collisionOffset, currentFrame, sheetSize, speed,
            millisecondsPerFrame)
        {

        }

    }
}
