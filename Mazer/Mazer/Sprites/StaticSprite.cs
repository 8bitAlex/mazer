﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Mazer.Sprites
{
    class StaticSprite : Sprite
    {
        public StaticSprite(Texture2D textureImage, Vector2 position ,Point frameSize,
            int collisionOffset, Point currentFrame, Point sheetSize, Vector2 speed) :
            base(textureImage, position, frameSize, collisionOffset, currentFrame, sheetSize, speed)
        {

        }

        public StaticSprite(Texture2D textureImage, Vector2 position ,Point frameSize,
            int collisionOffset, Point currentFrame, Point sheetSize, Vector2 speed, int millisecondsPerFrame)
            : base(textureImage, position, frameSize, collisionOffset, currentFrame, sheetSize, speed,
            millisecondsPerFrame)
        {

        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(textureImage, position, new Rectangle(currentFrame.X * frameSize.X,
                currentFrame.Y * frameSize.Y,
                frameSize.X, frameSize.Y),
                Color.White, 0, Vector2.Zero, 1f, SpriteEffects.None, 0.3f);
        }

        public override Vector2 Direction
        {
            get { return Vector2.Zero; }
        }
    }
}
