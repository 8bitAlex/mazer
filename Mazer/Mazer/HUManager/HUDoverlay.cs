﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Mazer.HUDManager
{
    public class HUDoverlay : Microsoft.Xna.Framework.DrawableGameComponent
    {
        private int attempts = 1;
        private int level = 0;
        private int timerSec = 0;
        private int timerMin = 0;
        private int timerHour = 0;
        private string attemptString = "Attempts ";
        private string levelString = "Level ";
        private string timerString;
        private int timeSinceLast;
        SpriteFont font;
        SpriteFont timerFont;
        SpriteBatch spriteBatch;
        Boolean stop;
        Boolean keyPressed;
        Boolean showScoreScreen;

        Texture2D introInstructions;
        Texture2D scoreBoard;

        public HUDoverlay(Game game)
            : base(game)
        {
            stop = false;
        }
        
        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(Game.GraphicsDevice);
            font = Game.Content.Load<SpriteFont>(@"fonts/HUDoverlay");
            timerFont = Game.Content.Load<SpriteFont>(@"fonts/TimerFont");
            introInstructions = Game.Content.Load<Texture2D>(@"images/instructions/move instuctions");
            scoreBoard = Game.Content.Load<Texture2D>(@"images/instructions/high score");
        }
        
        public override void Update(GameTime gametime)
        {
            timerString = timerHour + ":" + timerMin + ":" + timerSec;

            if(!stop)
                updateTimer(gametime);
                
        }

        public override void Draw(GameTime gameTime)
        {
            spriteBatch.Begin();
            
            // Attempt
            spriteBatch.DrawString(timerFont, attemptString, new Vector2(95, 576),
                Color.Orange, 0.0f, font.MeasureString(attemptString) / 2, 1.0f,
                SpriteEffects.None, 0f);

            spriteBatch.DrawString(timerFont, attempts.ToString(), new Vector2(160, 576),
                Color.Orange, 0.0f, font.MeasureString(attempts.ToString()) / 2, 1.0f,
                SpriteEffects.None, 0f);

            // Level
            spriteBatch.DrawString(timerFont, levelString, new Vector2(80, 11),
                Color.Orange, 0.0f, font.MeasureString(levelString) / 2, 1.0f,
                SpriteEffects.None, 0f);

            spriteBatch.DrawString(timerFont, level.ToString(), new Vector2(120, 11),
                Color.Orange, 0.0f, font.MeasureString(level.ToString()) / 2, 1.0f,
                SpriteEffects.None, 0f);

            // Timer
            spriteBatch.DrawString(timerFont, timerString, new Vector2(700, 575),
                Color.Orange, 0.0f, font.MeasureString(timerString) / 2, 1.0f,
                SpriteEffects.None, 0f);

            drawIntro(gameTime);

            if (showScoreScreen)
            {
                spriteBatch.Draw(scoreBoard, new Rectangle(250, 120, 300, 180), null, Color.White, 0.0f, Vector2.Zero, SpriteEffects.None, 0.0f);
                spriteBatch.DrawString(timerFont, getTime(), new Vector2(400, 210), Color.Orange);
                spriteBatch.DrawString(timerFont, getAttempts(), new Vector2(400, 183), Color.Orange);
            }

            spriteBatch.End();
            base.Draw(gameTime);
        }

        public void addAttempt()
        {
            attempts += 1;
        }

        public string getAttempts()
        {
            return attempts.ToString();
        }

        public void stopTimer()
        {
            stop = true;
        }

        public void reset()
        {
            attempts = 1;
            timerSec = 0;
            timerMin = 0;
            timerHour = 0;
            stop = false;
            keyPressed = true;
        }

        public string getTime()
        {
            return timerHour.ToString() + "h:" + timerMin.ToString() + "m:" + timerSec.ToString() + "s";
        }

        

        public void setLevel(int level)
        {
            this.level = level;
        }


        public void drawScore()
        {
            showScoreScreen = true;
        }

        public void hideScore()
        {
            showScoreScreen = false;
        }


        private void drawIntro(GameTime time)
        {
            isKeyPressed();
            if (time.TotalGameTime.Seconds < 10 && !keyPressed)
            {
                spriteBatch.Draw(introInstructions, new Rectangle(250, 120, 300, 180), null, Color.White, 0.0f, Vector2.Zero, SpriteEffects.None, 0);
            }
        }

        private void isKeyPressed()
        {
            if (Keyboard.GetState().IsKeyDown(Keys.Left) || Keyboard.GetState().IsKeyDown(Keys.A))
                keyPressed = true;
            if (Keyboard.GetState().IsKeyDown(Keys.Right) || Keyboard.GetState().IsKeyDown(Keys.D))
                keyPressed = true;
            if (Keyboard.GetState().IsKeyDown(Keys.Up) || Keyboard.GetState().IsKeyDown(Keys.W))
                keyPressed = true;
            if (Keyboard.GetState().IsKeyDown(Keys.Down) || Keyboard.GetState().IsKeyDown(Keys.S))
                keyPressed = true;

            GamePadState gamepadState = GamePad.GetState(PlayerIndex.One);
            if (gamepadState.ThumbSticks.Left.X != 0)
                keyPressed = true;
            if (gamepadState.ThumbSticks.Left.Y != 0)
                keyPressed = true;
        }


        private void updateTimer(GameTime gametime)
        {
            timeSinceLast += gametime.ElapsedGameTime.Milliseconds;
            if (timeSinceLast > 1000)
            {
                ++timerSec;
                timeSinceLast = 0;

                if (timerSec > 59)
                {
                    ++timerMin;
                    timerSec = 0;

                    if (timerMin > 59)
                    {
                        ++timerHour;
                        timerMin = 0;
                    }
                }

            }
        }

    }
}
