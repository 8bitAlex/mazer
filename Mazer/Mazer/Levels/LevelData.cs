﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Mazer.Sprites;

namespace Mazer.Levels
{
    class LevelData
    {
        Vector2 BLOCK_SPEED = new Vector2(5, 5);
        Point FRAME_SIZE = new Point(40, 40);

        Game game;
        Texture2D playerTexture;
        Texture2D scoreBlockTexture;
        Texture2D weightBlockTexture;
        Texture2D exitBlockTexture;
        Texture2D wallBlockTexture;

        Texture2D instructionWallTexture;
        Texture2D instructionScoreTexture;
        Texture2D instructionWeightTexture;
        Texture2D titleTexture;
        Texture2D instructionExitTexture;
        Texture2D instructionTip;

        public LevelData(Game game)
        {
            this.game = game;

            playerTexture = game.Content.Load<Texture2D>(@"images/blocks/player");
            scoreBlockTexture = game.Content.Load<Texture2D>(@"images/blocks/score block");
            weightBlockTexture = game.Content.Load<Texture2D>(@"images/blocks/weight block");
            exitBlockTexture = game.Content.Load<Texture2D>(@"images/blocks/exit block 2");
            wallBlockTexture = game.Content.Load<Texture2D>(@"images/blocks/wall 3");
            
            instructionWallTexture = game.Content.Load<Texture2D>(@"images/instructions/instruction_walls");
            instructionScoreTexture = game.Content.Load<Texture2D>(@"images/instructions/instructions_scoreBlock");
            instructionWeightTexture = game.Content.Load<Texture2D>(@"images/instructions/instruction_weightBlock");
            titleTexture = game.Content.Load<Texture2D>(@"images/instructions/title");
            instructionExitTexture = game.Content.Load<Texture2D>(@"images/instructions/instruction_exit");
            instructionTip = game.Content.Load<Texture2D>(@"images/instructions/tip");
        }

        public void loadSprites(List<Sprite> playerList, List<Sprite> spriteList, int level)
        {
            switch (level)
            {
                case 0 : loadLevelZero(playerList, spriteList);
                    break;
                case 1 : loadLevelOne(playerList, spriteList);
                    break;
                case 2 : loadLevelTwo(playerList, spriteList);
                    break;
                case 3 : loadLevelThree(playerList, spriteList);
                    break;
                case 4 : loadLevelFour(playerList, spriteList);
                    break;
                case 5: loadLevelFive(playerList, spriteList);
                    break;
                default: break;
            }
            
        }

        private void loadLevelZero(List<Sprite> playerList, List<Sprite> spriteList)
        {
            playerList.Add(new UserControlledSprite(playerTexture,
                new Vector2(395, 355), FRAME_SIZE, 3, new Point(0, 0), new Point(4, 1), BLOCK_SPEED, 500));

            spriteList.Add(new ScoreBlock(
                scoreBlockTexture,
                new Vector2(155, 145), FRAME_SIZE, 3, new Point(0, 0),
                new Point(1, 1), BLOCK_SPEED));

            spriteList.Add(new ScoreBlock(
                scoreBlockTexture,
                new Vector2(95, 145), FRAME_SIZE, 3, new Point(0, 0),
                new Point(1, 1), BLOCK_SPEED));

            spriteList.Add(new WeightBlock(
                weightBlockTexture,
                new Vector2(215, 205), FRAME_SIZE, 5, new Point(0, 0),
                new Point(1, 1), BLOCK_SPEED));

            //"MAZER"
            spriteList.Add(new StaticSprite(
                titleTexture,
                new Vector2(490, 90), new Point(210, 60), 0, new Point(0, 0),
                new Point(1, 1), new Vector2(0, 0)));

            //"Avoid these"
            spriteList.Add(new StaticSprite(
                instructionWallTexture,
                new Vector2(335,400),new Point(232,62),0,new Point(0,0),
                new Point(1,1),new Vector2(0,0)));

            //"Pick these up"
            spriteList.Add(new StaticSprite(
                instructionScoreTexture,
                new Vector2(185, 105), new Point(232, 62), 0, new Point(0, 0),
                new Point(1, 1), new Vector2(0, 0)));

            //"These are dead weight"
            spriteList.Add(new StaticSprite(
                instructionWeightTexture,
                new Vector2(100, 240), new Point(195, 90), 0, new Point(0, 0),
                new Point(1, 1), new Vector2(0, 0)));

            //"Bring blue blocks here to win"
            spriteList.Add(new StaticSprite(
                instructionExitTexture,
                new Vector2(530, 180), new Point(160, 110), 0, new Point(0, 0),
                new Point(1, 1), new Vector2(0, 0)));

            //"TIP: Player must enter exit zone"
            spriteList.Add(new StaticSprite(
                instructionTip,
                new Vector2(40, 460), new Point(380, 90), 0, new Point(0, 0),
                new Point(1, 1), new Vector2(0, 0)));


            generateExit(exitBlockTexture, spriteList, new Vector2(605, 295), 2, 4);


            generateWall(wallBlockTexture, spriteList, new Vector2(35, 25), 1, 18);
            generateWall(wallBlockTexture, spriteList, new Vector2(725, 25), 1, 18);
            generateWall(wallBlockTexture, spriteList, new Vector2(65, 25), 22, 1);
            generateWall(wallBlockTexture, spriteList, new Vector2(65, 535), 22, 1);
            generateWall(wallBlockTexture, spriteList, new Vector2(185, 385), 5, 1);
            generateWall(wallBlockTexture, spriteList, new Vector2(305, 205), 1, 6);
            generateWall(wallBlockTexture, spriteList, new Vector2(335, 205), 5, 1);

                
        }

        private void loadLevelOne(List<Sprite> playerList, List<Sprite> spriteList)
        {
            playerList.Add(new UserControlledSprite(playerTexture,
                new Vector2(215, 280), FRAME_SIZE, 3, new Point(0, 0), new Point(4, 1), BLOCK_SPEED, 500));

            spriteList.Add(new ScoreBlock(scoreBlockTexture,
                new Vector2(275, 205), FRAME_SIZE, 3, new Point(0, 0), new Point(1, 1), BLOCK_SPEED));

            spriteList.Add(new ScoreBlock(scoreBlockTexture,
                new Vector2(275, 355), FRAME_SIZE, 3, new Point(0, 0), new Point(1, 1), BLOCK_SPEED));

            generateExit(exitBlockTexture, spriteList, new Vector2(515, 235), 2, 4);

            //Left side walls
            generateWall(wallBlockTexture, spriteList, new Vector2(155, 115), 1, 12);

            //Top-left walls
            generateWall(wallBlockTexture, spriteList, new Vector2(185, 115), 7, 1);

            //top-mid walls
            generateWall(wallBlockTexture, spriteList, new Vector2(395, 115), 1, 4);
            generateWall(wallBlockTexture, spriteList, new Vector2(455, 175), 1, 2);

            //bottom-mid walls
            generateWall(wallBlockTexture, spriteList, new Vector2(395, 355), 1, 4);
            generateWall(wallBlockTexture, spriteList, new Vector2(455, 355), 1, 2);

            //bottom-left walls
            generateWall(wallBlockTexture, spriteList, new Vector2(185, 445), 7, 1);

            //mid-mid walls
            generateWall(wallBlockTexture, spriteList, new Vector2(395, 325), 3, 1);
            generateWall(wallBlockTexture, spriteList, new Vector2(395, 235), 3, 1);

            //right-top wall
            generateWall(wallBlockTexture, spriteList, new Vector2(455, 145), 5, 1);
            generateWall(wallBlockTexture, spriteList, new Vector2(455, 415), 5, 1);

            //back right wall
            generateWall(wallBlockTexture, spriteList, new Vector2(575, 175), 1, 8);
        }

        private void loadLevelTwo(List<Sprite> playerList, List<Sprite> spriteList)
        {
            playerList.Add(new UserControlledSprite(playerTexture,
                new Vector2(95, 85), FRAME_SIZE, 3, new Point(0, 0), new Point(4, 1), BLOCK_SPEED, 500));

            generateExit(exitBlockTexture, spriteList, new Vector2(305, 175), 1, 1);

            //Outer edge
            generateWall(wallBlockTexture, spriteList, new Vector2(35, 25), 1, 18);
            generateWall(wallBlockTexture, spriteList, new Vector2(725, 25), 1, 18);
            generateWall(wallBlockTexture, spriteList, new Vector2(65, 25), 22, 1);
            generateWall(wallBlockTexture, spriteList, new Vector2(65, 535), 22, 1);

            generateWall(wallBlockTexture, spriteList, new Vector2(155, 55), 1, 4);
            generateWall(wallBlockTexture, spriteList, new Vector2(125,145), 1, 11);
            generateWall(wallBlockTexture, spriteList, new Vector2(215, 415), 1, 4);
            generateWall(wallBlockTexture, spriteList, new Vector2(155, 325), 6, 1);
            generateWall(wallBlockTexture, spriteList, new Vector2(305, 355), 1, 4);
            generateWall(wallBlockTexture, spriteList, new Vector2(335, 445), 11, 1);
            generateWall(wallBlockTexture, spriteList, new Vector2(635, 325), 1, 4);
            generateWall(wallBlockTexture, spriteList, new Vector2(455, 235), 9, 1);
            generateWall(wallBlockTexture, spriteList, new Vector2(545, 265), 1, 4);
            generateWall(wallBlockTexture, spriteList, new Vector2(335, 325), 5, 4);
            generateWall(wallBlockTexture, spriteList, new Vector2(365, 145), 1, 6);
            generateWall(wallBlockTexture, spriteList, new Vector2(395, 145), 8, 1);
            generateWall(wallBlockTexture, spriteList, new Vector2(605, 115), 1, 1);
            generateWall(wallBlockTexture, spriteList, new Vector2(515, 55), 1, 1);
            generateWall(wallBlockTexture, spriteList, new Vector2(245, 115), 7, 1);
            generateWall(wallBlockTexture, spriteList, new Vector2(245, 145), 1, 3);
            generateWall(wallBlockTexture, spriteList, new Vector2(215, 235), 3, 1);
        }

        private void loadLevelThree(List<Sprite> playerList, List<Sprite> spriteList)
        {
            playerList.Add(new UserControlledSprite(playerTexture,
                new Vector2(250, 205), FRAME_SIZE, 3, new Point(0, 0), new Point(4, 1), BLOCK_SPEED, 500));

            spriteList.Add(new WeightBlock(
                weightBlockTexture,
                new Vector2(215, 265), FRAME_SIZE, 5, new Point(0, 0), new Point(1, 1), BLOCK_SPEED));

            spriteList.Add(new ScoreBlock(scoreBlockTexture,
                new Vector2(215, 355), FRAME_SIZE, 3, new Point(0, 0), new Point(1, 1), BLOCK_SPEED));

            spriteList.Add(new ScoreBlock(scoreBlockTexture,
                new Vector2(310, 205), FRAME_SIZE, 3, new Point(0, 0), new Point(1, 1), BLOCK_SPEED));

            generateExit(exitBlockTexture, spriteList, new Vector2(455, 175), 4, 2);

            generateWall(wallBlockTexture, spriteList, new Vector2(155, 145), 1, 10);
            generateWall(wallBlockTexture, spriteList, new Vector2(185, 145), 14, 1);
            generateWall(wallBlockTexture, spriteList, new Vector2(185, 415), 14, 1);
            generateWall(wallBlockTexture, spriteList, new Vector2(305, 325), 1, 3);
            generateWall(wallBlockTexture, spriteList, new Vector2(425, 175), 1, 4);
            generateWall(wallBlockTexture, spriteList, new Vector2(575, 175), 1, 9);
            generateWall(wallBlockTexture, spriteList, new Vector2(515, 235), 2, 1);
        }

        private void loadLevelFour(List<Sprite> playerList, List<Sprite> spriteList)
        {
            playerList.Add(new UserControlledSprite(playerTexture,
                new Vector2(635, 445), FRAME_SIZE, 3, new Point(0, 0), new Point(4, 1), BLOCK_SPEED, 500));

            generateExit(exitBlockTexture, spriteList, new Vector2(65, 55), 4, 3);

            generateScoreBlock(scoreBlockTexture, spriteList, new Vector2(515, 325), 1, 1);
            generateScoreBlock(scoreBlockTexture, spriteList, new Vector2(515, 445), 1, 1);
            generateScoreBlock(scoreBlockTexture, spriteList, new Vector2(635, 205), 1, 1);

            generateWeight(weightBlockTexture, spriteList, new Vector2(635, 325), 1, 1);
            generateWeight(weightBlockTexture, spriteList, new Vector2(515, 205), 1, 1);
            generateWeight(weightBlockTexture, spriteList, new Vector2(515, 205), 1, 1);
            generateWeight(weightBlockTexture, spriteList, new Vector2(425, 445), 1, 1);
            generateWeight(weightBlockTexture, spriteList, new Vector2(425, 325), 1, 1);
            generateWeight(weightBlockTexture, spriteList, new Vector2(425, 205), 1, 1);
            generateWeight(weightBlockTexture, spriteList, new Vector2(425, 85), 1, 1);
            generateWeight(weightBlockTexture, spriteList, new Vector2(185, 205), 1, 1);

            generateWall(wallBlockTexture, spriteList, new Vector2(575, 385), 1, 1);
            generateWall(wallBlockTexture, spriteList, new Vector2(455, 505), 1, 1);
            generateWall(wallBlockTexture, spriteList, new Vector2(695, 265), 1, 1);
            generateWall(wallBlockTexture, spriteList, new Vector2(545, 265), 1, 1);
            generateWall(wallBlockTexture, spriteList, new Vector2(335, 445), 1, 1);
            generateWall(wallBlockTexture, spriteList, new Vector2(335, 325), 1, 1);
            generateWall(wallBlockTexture, spriteList, new Vector2(335, 175), 1, 2);
            generateWall(wallBlockTexture, spriteList, new Vector2(185, 85), 1, 1);
            generateWall(wallBlockTexture, spriteList, new Vector2(185, 325), 1, 3);

            //outerwall
            generateWall(wallBlockTexture, spriteList, new Vector2(35, 25), 1, 18);
            generateWall(wallBlockTexture, spriteList, new Vector2(725, 25), 1, 18);
            generateWall(wallBlockTexture, spriteList, new Vector2(65, 25), 22, 1);
            generateWall(wallBlockTexture, spriteList, new Vector2(65, 535), 22, 1);
        }

        private void loadLevelFive(List<Sprite> playerList, List<Sprite> spriteList)
        {
            //playerList.Add(new UserControlledSprite(playerTexture,
            //    new Vector2(635, 445), FRAME_SIZE, 3, new Point(0, 0), new Point(4, 1), BLOCK_SPEED, 500));
        }
        


        /*
         *      Auto Generate Textures
         * */

        /// <summary>
        /// 
        /// </summary>
        /// <param name="wall"></param>
        /// <param name="list"></param>
        /// <param name="startLocatoin"></param>
        /// <param name="col"></param>
        /// <param name="row"></param>
        private void generateWall(Texture2D wall,List<Sprite> list, Vector2 startLocatoin, int col, int row)
        {
            for (int i = 0; i < row; i++)
            {
                for (int j = 0; j < col; j++)
                {
                    list.Add(new wallBlock(wall, new Vector2(startLocatoin.X + (j * 30), startLocatoin.Y + (i * 30)), FRAME_SIZE, 4,
                        new Point(0, 0), new Point(4, 1), BLOCK_SPEED, 200));
                }
            }
        }

        private void generateExit(Texture2D wall, List<Sprite> list, Vector2 startLocatoin, int col, int row)
        {
            for (int i = 0; i < row; i++)
            {
                for (int j = 0; j < col; j++)
                {
                    list.Add(new exitBlock(wall, new Vector2(startLocatoin.X + (j * 30), startLocatoin.Y + (i * 30)), FRAME_SIZE, 15,
                        new Point(0, 0), new Point(4, 1), BLOCK_SPEED, 200));
                }
            }
        }

        private void generateScoreBlock(Texture2D score, List<Sprite> list, Vector2 startLocatoin, int col, int row)
        {
            for (int i = 0; i < row; i++)
            {
                for (int j = 0; j < col; j++)
                {
                    list.Add(new ScoreBlock(score, new Vector2(startLocatoin.X + (j * 30), startLocatoin.Y + (i * 30)), FRAME_SIZE, 5,
                        new Point(0, 0), new Point(1, 1), BLOCK_SPEED));
                }
            }
        }

        private void generateWeight(Texture2D texture, List<Sprite> list, Vector2 startLocatoin, int col, int row)
        {
            for (int i = 0; i < row; i++)
            {
                for (int j = 0; j < col; j++)
                {
                    list.Add(new WeightBlock(texture,
                    new Vector2(startLocatoin.X + (j * 30), startLocatoin.Y + (i * 30)), FRAME_SIZE, 5, new Point(0, 0), new Point(1, 1), BLOCK_SPEED));
                }
            }
        }
    }
}
